import Vue from 'vue'
import VueResource from "vue-resource"
import VueRouter from "vue-router"
import ConfigRouter from "./router"
import FastClick from "fastclick"
import App from './App'
import {queryRouters} from "./vuex/actions/"
import store from "./vuex/store/"

document.addEventListener('DOMContentLoaded', function() {
  FastClick.attach(document.body);
}, false);

Vue.use(VueResource);
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VueRouter);
const router = new VueRouter();
ConfigRouter(router);

router.beforeEach(function (transition) {
	if(store.state.routers.length==0){
  		queryRouters(store);
	}
  	transition.next();
});

const webApp = Vue.extend(App);
router.start(webApp, '#app');




